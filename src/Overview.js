import { API_ROOT } from './api-config';
import React from 'react';
import {useEffect, useState} from 'react';
import { Link } from 'react-router-dom'
import './App.css';

function Overview() {
  const [state, setState] = useState({ data: {}, loaded: false});

  useEffect(() => {
    fetch(`${API_ROOT}/songbooks`)
      .then(response => response.json())
      .then(json => {
        console.log(json);
        setState({
          data: json,
          loaded: true
        });
      });
  }, []);

  const entries = ('songbooks' in state.data) ? Object.keys(state.data['songbooks']).map((key) => {
      return <li key={key}><Link to={key + '/'}>{state.data['songbooks'][key]}</Link></li>;
  }) : null;

  return (
      <ul>
        {entries}
      </ul>);
}

export default Overview;