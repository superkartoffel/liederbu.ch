import React from 'react';

export default function fileUpload(props) {
    let validateSize = (event) => {
        let file = event.target.files[0];
        let size = 10 * 1024 * 1024;
        let err = '';
        console.log(file.size);
        if (file.size > size) {
            err = file.type + ' is too large, please pick a smaller file\n';
            alert(err);
        }
        return true
    };

    let onChangeHandler = (event, redirect) => {
        var file = event.target.files[0];
        if (validateSize(event)) {
            console.log(file);
            // if return true allow to setState
            fileUploadHandler(file, redirect);    
        }
    };
    
    let fileUploadHandler = (selectedFile, redirect) => {
        const data = new FormData()
        data.append('file', selectedFile)
        console.log(data);

        fetch(props.targetPath, {method: 'POST', body: data})
        .then(response => {
          if (!response.ok) {
            throw Error(response.statusText);
          }
          return response.json()
        })
        .then(json => {
          console.log(json);
          redirect(json.redirect);
        }).catch(function(error) {
          console.log(error);
        });
    
    };

    return (
        <div className="input-group">
            <div className="input-group-prepend">
                <span className="input-group-text" id="inputGroupFileAddon01">Upload</span>
            </div>
            <div className="custom-file">
                <input type="file" className="custom-file-input" id="inputGroupFile01"
                    aria-describedby="inputGroupFileAddon01" multiple accept="image/jpeg,.pdf" 
                    onChange={(e) => { onChangeHandler(e, props.redirect)} } />
                <label className="custom-file-label" htmlFor="inputGroupFile01">Choose files</label>
            </div>
        </div>);
}