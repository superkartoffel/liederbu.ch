import React from 'react';
import { Link } from 'react-router-dom'
import EditableLabel from 'react-editable-label';
import EditableStanza from './EditableStanza';
import FileUpload from './FileUpload';

function NavBar(props) {
    const notationBtn = props.hasNotation ? 
      <span onClick={(e) => { props.onClick()}} className="btn btn-secondary floatRight">
        <input type="checkbox" name="notationOn" checked={props.notationOn} onChange={(e) => { props.onClick()}} /> Noten
      </span> : null;
  
    return (
      <div>
        <Link to={`/${props.setName}/`} role="button" className="btn btn-secondary">Zur&uuml;ck</Link>
        {notationBtn}
      </div>
    );
  }

function NewSong(props) {
    return (
        <div className="Song">
          <NavBar 
            setName={props.setName} 
            hasNotation={false} />
          <h4><EditableLabel initialValue="Neues Lied" save={props.saveNewSong} /></h4>
        </div>); 
}

export default function Song (props) {
  const songName = props.match.params.songName;
  if (songName in props.songs) {
    const song = props.songs[songName];
    const showNotation = "hasNotation" in song && song.hasNotation && props.notationOn;
    const lyrics = song.lyrics ? song.lyrics.map((stanza, i) => 
      ( (showNotation && i===0) ? null :
        <li key={i}>
        {props.editable ? 
            <EditableStanza initialValue={stanza}
                save={text => props.saveStanza(songName, i, text)} 
                labelClass="whiteSpacePre"
                inputClass="form-control"
                saveLabel="Speichern"
                saveBtnClass="btn btn-primary"
                cancelLabel="Abbrechen"
                cancelBtnClass="btn btn-secondary"
                /> : 
            <div className="whiteSpacePre">{stanza}</div> }
      </li>)) : null;

    const imageTags = [];
    if ("images" in song) {
      for (let i=0;i<song.images.length; i++) { 
       imageTags.push(<img key={i} src={'/img/' + song.images[i]} alt="" />);
      }
    }
    const images = "images" in song ? 
      <div>
        { imageTags }
      </div> : null;
    
    const attachments = song.attachments.map(a => {
      return <li key={a.filename}><a href={"/img/" + a.filename}>{a.name}</a></li>;
    });

    let url = ['img', props.match.params.setName, props.match.params.songName].join('/');
    const notation = showNotation ?
      (<div><img src={`/${url}/notationsmall.svg`} alt={song.lyrics[0]} className="smallimg" />
      <img src={`/${url}/notationmedium.svg`} alt={song.lyrics[0]} className="midimg" />
      <img src={`/${url}/notationlarge.svg`} alt={song.lyrics[0]} className="bigimg" /></div>) : null;

    const additionalInfo = song.additionalInfo ? 
    <div className="textRight">
      <a className="px-3 lead" data-toggle="collapse" href="#additionalInfo" aria-expanded="false" aria-controls="additionalInfo">&#9974;</a>
      <div className="collapse" id="additionalInfo">
        <div className="card card-body textLeft">
          {Object.keys(song.additionalInfo).map((key, i) => 
            <div key={key}>{key}: {song.additionalInfo[key]}</div>)}
        </div>
      </div>
    </div> : null;

    const fileUpload = props.editable ? 
      <FileUpload redirect={props.redirect} targetPath={`/v1/songbooks/${props.match.params.setName}/${songName}/attachments`} />
    : null;

    return (
      <div className="Song">
        <NavBar 
          setName={props.match.params.setName} 
          onClick={props.notationOnHandler}
          notationOn={props.notationOn} 
          hasNotation={"hasNotation" in song && song.hasNotation} />
        {additionalInfo}
        <h4>{props.editable ?
          <EditableLabel initialValue={song.title} save={(newTitle) => props.saveSongTitle(songName, newTitle)} />
          : song.title }
        </h4>
        <div className="notation">{notation}</div>
        <ol start={showNotation ? 2 : 1} className="lyrics">
            {lyrics}
        </ol>
        {props.editable ? <button onClick={() => props.addStanza(songName)} className="btn btn-primary">Strophe hinzuf&uuml;gen</button> : null }
        
        {attachments.length > 0 ? <div className="my-2"><ul>{attachments}</ul></div> : null }
        <div className="my-2">{images}</div>
        <div className="my-2">{fileUpload}</div>
      </div>);
  }

  return <NewSong saveNewSong={props.saveNewSong} setName={props.match.params.setName} />;
}