import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';
import EditableLabel from 'react-editable-label';

export default function SongList(props) {
  const entries = props.loaded ?   
    Object.keys(props.songs).sort().map((key, index) => {
      const element = props.songs[key];
      return (
          <Link to={`${props.match.url}${key}/`}
            className="list-group-item list-group-item-action"
            key={key}>
              {element.title}
            </Link>);
    })
    : 
    (<div className="spinner-border" role="status">
      <span className="sr-only">Loading...</span>
    </div>);
    
  return (
    <div className="SongList">
      <h3 className="serif-centered my-3">
      { (props.heading && props.saveHeading) ? 
        props.editable ? <EditableLabel initialValue={props.heading} save={props.saveHeading} /> : props.heading
        : "loading..." }
      </h3>
      <div className="list-group">
        {entries}
      </div>
      { props.editable ? <Link to={`${props.match.url}new/`} className="btn btn-primary">Neues Lied hinzuf&uuml;gen</Link> : null }
    </div>
  )
}