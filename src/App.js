import { API_ROOT } from './api-config';
import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import Overview from './Overview';
import SongList from './SongList';
import Song from './Song';
import Login from './Login';

class StateHolder extends React.Component {
  state = {
    data: {title: "", songs: {}},
    loaded: false,
    notationOn: true,
    redirect: false,
    editMode: false,
    loggedIn: false,
    loginRequired: false,
  };

  componentDidMount = () => {
    this.loadData();
  }

  loadData = () => {
    if (this.props.match.params.setName) {
      fetch(`${API_ROOT}/songbooks/${this.props.match.params.setName}`, {credentials: "include"})
        .then(response => {
          console.log(response);
          if (response.ok) {
            return response.json();
          }else if (response.status === 403){
            this.setState({loggedIn: false, loginRequired: true});
          }else{
            console.log("Unexpected response from API: " + response.status);
          }
          return Promise.reject("No data received");
        })
        .then(json => {
          console.log(json);
          this.setState({
            data: json,
            loaded: true
          });
        }).catch((e)=>console.log(e));
    }
  }

  notationOnHandler = () => {
    this.setState({notationOn: !this.state.notationOn})
  };

  saveHeading = (newValue) => {
    if (this.props.match.params.setName) {
      var formData = new FormData();
      formData.append('url', this.props.match.params.setName);
      formData.append('name', newValue);

      fetch(`${API_ROOT}/songbooks/${this.props.match.params.setName}`, {method: 'PUT', body: formData})
        .then(response => {
          if (!response.ok) {
            throw Error(response.statusText);
          }
          return response.json()
        })
        .then(json => {
          console.log(json);
          this.setState({
            data: json,
            loaded: true
          });
        }).catch(function(error) {
          console.log(error);
        });
    }
  }

  saveNewSong = (newSong) => {
    if (this.props.match.params.setName) {
      var formData = new FormData();
      formData.append('title', newSong);

      fetch(`${API_ROOT}/songbooks/${this.props.match.params.setName}`, {method: 'POST', body: formData})
        .then(response => {
          if (!response.ok) {
            throw Error(response.statusText);
          }
          return response.json()
        })
        .then(json => {
          console.log(json);
          this.setState({
            data: json.data,
            loaded: true,
            redirect: json.redirect
          });
        }).catch(function(error) {
          console.log(error);
        });
    }
  }

  saveSongTitle = (oldTitle, newTitle) => {
    if (this.props.match.params.setName && oldTitle && newTitle) {
      let song = this.state.data.songs[oldTitle];
      var formData = new FormData();
      formData.append('title', newTitle);
      song.lyrics.forEach((stanza, i) => 
        formData.append('stanza_' + i, stanza));

      fetch(`${API_ROOT}/songbooks/${this.props.match.params.setName}/${oldTitle}`, {method: 'PUT', body: formData})
        .then(response => {
          if (!response.ok) {
            throw Error(response.statusText);
          }
          return response.json()
        })
        .then(json => {
          console.log(json);
          this.redirect(json.redirect);
        }).catch(function(error) {
          console.log(error);
        });
    }
  }

  redirect = (target) => {
    this.loadData();
    this.setState({
      redirect: target
    });
  }

  saveStanza = (songName, i, newStanza) => {
    if (this.props.match.params.setName && songName) {
      let song = this.state.data.songs[songName];
      song.lyrics[i] = newStanza;
      var formData = new FormData();
      formData.append('title', song.title);
      song.lyrics.forEach((stanza, i) => 
        formData.append('stanza_' + i, stanza));

      fetch(`${API_ROOT}/songbooks/${this.props.match.params.setName}/${songName}`, {method: 'PUT', body: formData})
        .then(response => {
          if (!response.ok) {
            throw Error(response.statusText);
          }
          return response.json()
        })
        .then(json => {
          console.log(json);
          this.loadData();
        }).catch(function(error) {
          console.log(error);
        });
    }
  }

  addStanza = (songName) => {
    let newData = this.state.data;
    newData.songs[songName].lyrics.push("Neue Strophe");
    this.setState({
      data: newData
    });
  }

  render() {
    if (this.state.redirect) {
      let target = this.state.redirect;
      this.setState({redirect: false});
      return <Redirect to={target} />
    }

    return (
      <>
      <Login foreground={this.state.loginRequired} />
      <div className="Songbook">
      <Route exact path='/:setName/:songName/' render={(props) => 
        <Song 
          songs={this.state.data.songs} 
          notationOnHandler={this.notationOnHandler} 
          notationOn={this.state.notationOn}
          saveNewSong={this.saveNewSong}
          saveStanza={this.saveStanza}
          addStanza={this.addStanza}
          saveSongTitle={this.saveSongTitle}
          editable={this.state.editMode}
          redirect={this.redirect}
          {...props} />} />
      <Route exact path='/:setName/' render={(props) => 
        <SongList
          heading={this.state.data.title} 
          saveHeading={this.saveHeading}
          songs={this.state.data.songs} 
          loaded={this.state.loaded}
          editable={this.state.editMode}
          {...props} />} />
      </div>
      </>);
  }
}

function App() {
  return (
    <Router>
      <Route path='/:setName/' component={StateHolder} />
      <Route exact path='/' component={Overview} />
    </Router>
    );
}

export default App;
