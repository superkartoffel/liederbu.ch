import React from 'react';
import PropTypes from 'prop-types';

export default class EditableStanza extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 'label',
            value: '',
            previous: ''
        };

        this.handleKeyUp = this.handleKeyUp.bind(this);
    }

    async componentWillMount() {
        const { initialValue } = this.props;
        const { value, previous } = this.state;

        if (value === '') {
            await this.changeValue(initialValue);
        }

        if (previous === '') {
            await this.changePrevious(initialValue);
        }
    }

    componentDidUpdate() {
        const { view } = this.state;

        if (view === 'text') {
            this.textInput.focus();
        }
    }

    switchView(view) {
        this.setState({
            view
        });
    }

    changePrevious(previous) {
        return new Promise((resolve, reject) => {
            this.setState({
                previous
            }, () => {
                resolve();
            });
        });
    }

    changeValue(value) {
        return new Promise((resolve, reject) => {
            this.setState({
                value
            }, () => {
                resolve();
            });
        });
    }

    async handleKeyUp(e) {
        const { previous } = this.state;
        const { disableKeys } = this.props;

        if (disableKeys === true) {
            return;
        }

        //We need this otherwise React squawks at accessing the event in an async function
        e.persist();

        if (e.key === 'Escape') {
            await this.changeValue(previous);

            this.switchView('label');
        }
    }

    renderInput() {
        const { value } = this.state;
        const { save, inputClass, saveLabel, saveBtnClass, cancelLabel, cancelBtnClass } = this.props;

        const numLine = value.split("\n").length;
        return (
            <div className="test">
                <textarea
                    rows={numLine}
                    ref={input => this.textInput = input}
                    className={inputClass !== undefined ? inputClass : ''}
                    onChange={e => {
                        this.changeValue(e.target.value);
                    }}
                    // onBlur={e => {
                    //     this.switchView('label');
                    //     this.changePrevious(e.target.value);
                    //     save(e.target.value);
                    // }}

                    onKeyUp={this.handleKeyUp}
                    value={value}
                ></textarea>
                <button 
                    className={saveBtnClass}
                    onClick={e => {
                        this.switchView('label');
                        this.changePrevious(this.state.value);
                        save(this.state.value);
                    }} >{saveLabel}</button>
                <button 
                    className={cancelBtnClass}
                    onClick={e => {
                        this.changeValue(this.state.previous);
                        this.switchView('label');
                    }} >{cancelLabel}</button>
            </div>
        );
    }

    renderLabel() {
        const { value } = this.state;
        const { labelClass } = this.props;

        return (
            <div className={labelClass !== undefined ? labelClass : ''} onClick={e => {
                    this.switchView('text');
                }}>{value}
            </div>
        );
    }

    render() {
        const { view } = this.state;

        if (view === 'label') {
            return this.renderLabel();
        } else {
            return this.renderInput();
        }
    }
}

EditableStanza.propTypes = {
    initialValue: PropTypes.string.isRequired,
    save: PropTypes.func.isRequired,
    labelClass: PropTypes.string,
    inputClass: PropTypes.string,
    disableKeys: PropTypes.bool
}
