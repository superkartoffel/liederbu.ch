import React from 'react';
import './Login.css';

function Login(props) {
    return (
    <div id={props.foreground ? "signin-box" : "hidden"}>
        <form class="form-signin">
        {/* <img class="mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" /> */}
        <h1 class="h3 mb-3 font-weight-normal">Anmelden:</h1>
        <label for="inputEmail" class="sr-only">E-Mail-Adresse</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="E-Mail-Adresse" required autofocus />
        <label for="inputPassword" class="sr-only">Passwort</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Passwort" required />
        <div class="checkbox mb-3">
            <label>
            <input type="checkbox" value="remember-me" /> angemeldet bleiben
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Anmelden</button>
        </form>
    </div>);
}

export default Login;