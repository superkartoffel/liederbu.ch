#!/usr/bin/env python3
from flask import Flask, request
from flask_restful import reqparse, abort, Resource, Api
from sqlalchemy import create_engine
from json import dumps
from flask_jsonpify import jsonify
import re
from werkzeug import secure_filename
import os.path
import string
import random

db_connect = create_engine('sqlite:///songbook.db')
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = '/home/hvorwerk/liederbu.ch/img/'
app.config['TMP_FOLDER'] = '/home/hvorwerk/liederbu.ch-api-v1/tmp/'
app.config['MAX_CONTENT_PATH'] = 10 * 1024 * 1024
api = Api(app)

def id_generator(size=10, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
   return ''.join(random.choice(chars) for _ in range(size))
   
def abort_if_songbook_does_exist(conn, songbook_id):
    query = conn.execute("select intId from songbooks WHERE id = ?", (songbook_id))
    if len(query.cursor.fetchall()) > 0:
        abort(400, message={"url": "Songbook {} does already exist".format(songbook_id)})

def abort_if_song_does_not_exist(conn, song_id):
    query = conn.execute("select intId from songs WHERE id = ?", (song_id))
    result = query.cursor.fetchall()
    if len(result) == 0:
        abort(400, message={"url": "Song {} does not exist".format(song_id)})
    else:
        return result[0][0]
        
def abort_if_song_does_exist(conn, song_id):
    query = conn.execute("select intId from songs WHERE id = ?", (song_id))
    result = query.cursor.fetchall()
    if len(query.cursor.fetchall()) > 0:
        abort(400, message={"url": "Song {} does already exist".format(song_id)})

def abort_if_songbook_does_not_exist(conn, songbook_id):
    query = conn.execute("select intId from songbooks WHERE id = ?", (songbook_id))
    result = query.cursor.fetchall()
    if len(result) == 0:
        abort(400, message={"url": "Songbook {} does not exist".format(songbook_id)})
    else:
        return result[0][0]

songbook_parser = reqparse.RequestParser()
songbook_parser.add_argument('url', required=True, help="Cannot be blank")
songbook_parser.add_argument('name', required=True, help="Cannot be blank")

class Songbooks(Resource):
    def get(self):
        conn = db_connect.connect()
        query = conn.execute("select id, name from songbooks WHERE public = 1")
        return {'songbooks': {i[0]: i[1] for i in query.cursor.fetchall()}}
        
    def post(self):
        conn = db_connect.connect()
        args = songbook_parser.parse_args()
        abort_if_songbook_does_exist(conn, args['url'])
        query = conn.execute("insert into songbooks (id, name, public) VALUES (?,?,1)", (args['url'], args['name']))
        return self.get()

song_parser = reqparse.RequestParser()
song_parser.add_argument('title', required=True, help="Cannot be blank")

class Songbook(Resource):
    def get(self, songbook_id):
        conn = db_connect.connect()
        abort_if_songbook_does_not_exist(conn, songbook_id)
        query = conn.execute("select intId, name from songbooks WHERE id = ?", (songbook_id))
        (intId, title) = query.cursor.fetchall()[0]
        query = conn.execute("select id, title, hasNotation, intId from songs WHERE songbook_id = ?", (intId))
        songs = {}
        result = query.cursor.fetchall()
        for i in result:
            songs[i[0]] = {'title': i[1], 'hasNotation': i[2]}
            lyricsQuery = conn.execute("select text from lyrics WHERE song_id = ? ORDER BY stanza", (i[3]))
            l = lyricsQuery.cursor.fetchall()
            songs[i[0]]['lyrics'] = [item for sublist in l for item in sublist] # flatten the list
            
            
            attachmentsQuery = conn.execute("select name, filename, isImage from attachments WHERE song_id = ? ORDER BY position", (i[3]))
            a = attachmentsQuery.cursor.fetchall()
            songs[i[0]]['images'] = [row[1] for row in a if row[2] == 1]
            songs[i[0]]['attachments'] = [{'name': row[0], 'filename': row[1]} for row in a if row[2] == 0]
        return {'title': title, 'songs': songs}
       
    def put(self, songbook_id):
        conn = db_connect.connect()
        args = songbook_parser.parse_args()
        songbook_intId = abort_if_songbook_does_not_exist(conn, songbook_id)
        if (args['url'] != songbook_id):
            abort(400, message={"url": "Songbook {} does not match url".format(songbook_id)})
        
        query = conn.execute("update songbooks set name = ? WHERE id = ?", (args['name'], songbook_id))
        return self.get(songbook_id)
            
    def post(self, songbook_id):
        conn = db_connect.connect()    
        songbook_intId = abort_if_songbook_does_not_exist(conn, songbook_id)
        args = song_parser.parse_args()
        clean_title = re.sub('[^a-zA-Z0-9]', '_', args['title'])
        abort_if_song_does_exist(conn, clean_title)
        query = conn.execute("insert into songs (id, title, songbook_id) VALUES (?,?,?)", (clean_title, args['title'], songbook_intId))
        i=0
        song_id = query.lastrowid 
        while "stanza_%d" % i in request.form:
            query = conn.execute("insert into lyrics (stanza, text, song_id) VALUES (?,?,?)", (i, request.form["stanza_%d" % i], song_id))
            i = i + 1
        return {'redirect': "/%s/%s/" %(songbook_id, clean_title), 
                'data': self.get(songbook_id) }
    
    
class Song(Resource):
    def put(self, songbook_id, song_id):
        conn = db_connect.connect()    
        songbook_intId = abort_if_songbook_does_not_exist(conn, songbook_id)
        song_intId = abort_if_song_does_not_exist(conn, song_id)
        args = song_parser.parse_args()
        clean_title = re.sub('[^a-zA-Z0-9]', '_', args['title'])
        abort_if_song_does_exist(conn, clean_title)
        query = conn.execute("update songs set title = ?, id = ? WHERE intId = ?", (args['title'], clean_title, song_intId))
        query = conn.execute("delete from lyrics WHERE song_id = ?", (song_intId))
        i=0
        while "stanza_%d" % i in request.form:
            query = conn.execute("insert into lyrics (stanza, text, song_id) VALUES (?,?,?)", (i, request.form["stanza_%d" % i], song_intId))
            i = i + 1
        return {'redirect': "/%s/%s/" %(songbook_id, clean_title) }

class Attachment(Resource):
    def post(self, songbook_id, song_id):
        conn = db_connect.connect()    
        songbook_intId = abort_if_songbook_does_not_exist(conn, songbook_id)
        song_intId = abort_if_song_does_not_exist(conn, song_id)
        f = request.files['file']
        filename, file_extension = os.path.splitext(secure_filename(f.filename))        
        targetFilename = id_generator() + file_extension
        targetFilepath = os.path.join(app.config['UPLOAD_FOLDER'], targetFilename)
        while os.path.exists(targetFilepath):
            targetFilename = id_generator() + file_extension
            targetFilepath = os.path.join(app.config['UPLOAD_FOLDER'], targetFilename)
        f.save(targetFilepath)
        # get position
        query = conn.execute("select position from attachments WHERE song_id = ? ORDER BY position", (song_intId))
        lastPositions = query.cursor.fetchall()
        if len(lastPositions) == 0:
            lastPosition = 0
        else:
            lastPosition = lastPositions[0][0]
        query = conn.execute("insert into attachments (song_id, position, name, filename, isImage) VALUES (?,?,?,?,?)", (song_intId, lastPosition+1, secure_filename(f.filename), targetFilename, file_extension.lower() == '.jpg'))
        return {'redirect': "/%s/%s/" %(songbook_id, song_id) }
    
api.add_resource(Songbooks, '/v1/songbooks')
api.add_resource(Songbook, '/v1/songbooks/<songbook_id>')
api.add_resource(Song, '/v1/songbooks/<songbook_id>/<song_id>')
api.add_resource(Attachment, '/v1/songbooks/<songbook_id>/<song_id>/attachments')

if __name__ == '__main__':
    app.run()
