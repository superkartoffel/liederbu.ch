CREATE TABLE songbooks(
        intId INTEGER PRIMARY KEY,
        id TEXT NOT NULL,
        name TEXT NOT NULL,
        public INTEGER '0'
);
CREATE TABLE songs(
        intId INTEGER PRIMARY KEY,
        title TEXT NOT NULL,
        id TEXT NOT NULL,
        songbook_id INTEGER NOT NULL,
        hasNotation INTEGER '0'
);
CREATE TABLE lyrics(
        id INTEGER PRIMARY KEY,
        song_id INTEGER NOT NULL,
        stanza INTEGER NOT NULL,
        text TEXT
);
CREATE TABLE attachments(
        id INTEGER PRIMARY KEY,
        song_id INTEGER NOT NULL,
        position INTEGER NOT NULL,
        name TEXT,
        filename TEXT NOT NULL,
        isImage INTEGER '0'
);

INSERT INTO songbooks (intId, id, name, public) VALUES (1, '3bataillon', 'III. Bataillon', 1);
INSERT INTO songs (intId, id, title, songbook_id, hasNotation) VALUES (1, 'lohner_lied', 'Lohner Lied', 1, 1);
INSERT INTO lyrics (song_id, stanza, text) VALUES (1, 0, 'Aller deutschen Städte...');
INSERT INTO lyrics (song_id, stanza, text) VALUES (1, 1, 'Gute Menschen treu und...');
INSERT INTO lyrics (song_id, stanza, text) VALUES (1, 2, 'Darum sehn ich mich...');
