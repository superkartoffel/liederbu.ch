import os
import json
import yaml
from string import Template
from subprocess import check_call

#lilypond template
lilypond_template = Template('''
\\version "2.19.82"
\header {
  tagline = ""
}
#(set! paper-alist (cons '("my size" . (cons (* $size cm) (* 120 cm))) paper-alist))
\paper {
  #(set-paper-size "my size")
}
$notation
''')

for setDir in ['brink-nord']: #os.listdir('data'):
  if not os.path.isdir('/'.join(['data', setDir])):
    print("skipping " + setDir)
    continue # skip files

  # open output file
  print("opening output file.")
  os.makedirs('/'.join(['public', setDir]))
  
  with open('/'.join(['public', setDir,'data.json']), 'w') as outFile:
    # read song set meta data

    try:
      print("finding set metadata: " + '/'.join(['data', setDir, 'meta.yaml']))
      with open('/'.join(['data', setDir, 'meta.yaml']), "r") as setMetaFile:
        setMetaData = yaml.load(setMetaFile, Loader=yaml.FullLoader)
        setMetaData['songs'] = {}
        
        # iterate all directories (songs)
        for songDir in os.listdir('/'.join(['data', setDir])):
          if not os.path.isdir('/'.join(['data', setDir, songDir])):
            print("skipping " + songDir)
            continue # skip files
            
          print("adding song " + songDir)
          with open('/'.join(['data', setDir, songDir, 'meta.yaml']), 'r') as songMetaFile:
            setMetaData['songs'][songDir] = yaml.load(songMetaFile, Loader=yaml.FullLoader)
            print('as ' + setMetaData['songs'][songDir]['title'])
    
          # check if notation is available
          if os.path.isfile('/'.join(['data', setDir, songDir, 'notation.txt'])):
            setMetaData['songs'][songDir]['hasNotation'] = True
            print("Calling lilypond.")
            with open('/'.join(['data', setDir, songDir, 'notation.txt']), 'r') as notationFile:
              notationText = notationFile.read()
              os.makedirs('/'.join(['public', setDir, songDir]))
              for s in [['11', 'small'], ['16', 'medium'], ['22', 'large']]:
                with open('tmp.ly', 'w') as tmpLily:
                  tmpLily.write(
                    lilypond_template.substitute(
                      size=s[0], 
                      notation=notationText))
                check_call([
                  "lilypond", 
                  "-dpoint-and-click", 
                  "-ddelete-intermediate-files", 
                  "-dbackend=svg", 
                  "tmp.ly"])
                check_call(["sed", "-i", "s/font-family=\"serif\"/font-family=\"sans-serif\"/g", "tmp.svg"])
                check_call(["inkscape", "-f", "tmp.svg", "-D", "-l", "tmp.svg"])
                os.rename("tmp.svg", '/'.join(['public', setDir, songDir, 'notation' + s[1] + '.svg']))
                
              os.remove('tmp.ly')
        json.dump(setMetaData, outFile)        
    except yaml.YAMLError as exc:
      print(exc)
      
    
