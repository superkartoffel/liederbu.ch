\score {
  <<
    \new Voice = "melody" { 
    \key a \major
    \time 3/4
    \relative {
      \repeat volta 2 {
      \partial 4 { e'8 e8 | }
      a4 a4 cis8( a8) | e2 e4 | d4 b4 e4 |
      cis4 a4 } a'4 | gis4 b4. gis8 | a4 cis4 a4 |
      gis4 b4. gis8 | a4 cis4 a4 | fis4 fis4 a8( fis8) |
      e2 e8 cis'8 | cis8( b8) a4 gis4 | a2 r4 | \bar "|."
    }
    }
    
    \new Lyrics \lyricsto "melody" {
      <<
      { Die Ge -- | dan -- ken sind | frei, wer | kann sie er -- | ra -- ten, }
      \new Lyrics 
      { 
      \set associatedVoice = "melody"
      sie _ | flie -- hen vor -- | bei wie | nächt -- li -- che | Schat -- ten.}
      >>
      
      Kein | Mensch kann sie | wis -- sen, kein | Jä -- ger er -- | 
      schie -- ßen. Es | blei -- bet da -- | bei: Die Ge -- | 
      dan -- ken sind | frei. |
      
    }
    >>
  }
