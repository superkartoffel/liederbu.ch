\score {
  <<
    { 
    \time 3/4
    \key c \major
    \relative {
      \partial 4 { g'4 | }
      c2 a4 | g2 g4 | e2.( | e2) g4 | a2 c4 | b2 g4 | f2.( | f2) d'4 |
      d2 c4 | b2 a4 | g2 g4 | a2 b4 | c2 a4 | g2.( | g2) r4 | c2 a4 | g2 g4 |
      e2.( | e2) e4 | g2 c4 | b2 g4 | f2.( | f2) d'4 | 
      d2 c4 | b2 a4 | g2 g4 | a2 b4 | c2.( | c2.) \bar "|."
    }
    }
    
    \addlyrics {
      Die Gam -- serln schwarz und braun
      de san so liab zan schaun,
      und wennst as schias -- sen willst
      do muasst di au -- fi traun.
      Denn se san so gschwind,
      se hom oan glei im Wind,
      se fan -- gens pfei -- fen on
      und san da -- von.
    }
    >>
  }
