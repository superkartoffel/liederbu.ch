\score {
  <<
    \new Voice = "melody" { 
    \key a \major
    \relative {
      \repeat volta 2 {
      e'8. e16 a8 gis8 fis8 gis8 a8 e8 | b'8. b16 b8 a8
      gis8 fis8 e4 | }
      \repeat volta 2 {
      cis'8. cis16 cis8 cis8 cis8 e,8 e8 r8 | cis'8. b16 a8 b8
      cis4 a4 }
      a4. a8 gis4 fis4 | gis2 cis,4 r4 | fis2 e4 d4 |
      cis4 fis4 e4 r4 | cis'4. cis8 d4 cis4 | fis,4( cis'4) b4
      \tuplet 3/2 { e,8 fis gis } | a4
      \tuplet 3/2 { e8 fis gis } a4
      \tuplet 3/2 { e8 fis gis } | a8. a16 a8 a8 a4 r4 | \bar "|."
    }
    }
    
    \new Lyrics \lyricsto "melody" {
      <<
      { Als die Rö -- mer frech ge -- wor -- den, 
      sim -- se -- rim -- sim -- sim -- sim -- sim,
      Vor -- ne beim Trom -- pe -- ten -- schall,}
      \new Lyrics 
      { 
      \set associatedVoice = "melody"
      zo -- gen sie nach Deutsch -- lands Nor -- den,
      sim -- se -- rim -- sim -- sim -- sim -- sim.
      ritt Herr Ge -- neral -- feld -- mar -- schall,
      }
      >>
      tä -- te -- rä -- tä -- tä -- tä,
      Herr Quin -- ti -- lius Va -- rus, 
      wau wau wau wau wau wau,
      Herr Quin -- ti -- lius Va -- rus, 
      schnät -- te -- räng -- täng,
      schnät -- te -- räng -- täng,
      schnät -- te -- räng -- täng,
      de -- räng -- täng -- täng.
    }
    >>
  }
