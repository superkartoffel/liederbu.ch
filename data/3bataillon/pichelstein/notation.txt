\score {
  <<
    { 
    \key c \major
    \relative {
      \partial 4 { c'8 d | }
      e2. d8 e | f2 r8 f8 e f | g4 g a a | d,2.
      c8 d | e2. f8 g | a2( c) | g4 e8 c d4 g4 | c,1 |
      
      \repeat volta 2 {
      c'8 c c a c c c a | g1 | g8 g g e g g g e | c1 |
      }
    }
    }
    
    \addlyrics {
      Pi -- chel -- stein, Pi -- chel -- stein,
      wir tre -- ten für -- ein -- an -- der ein.
      Pi -- chel -- stein, Pi -- chel -- stein,
      wir wol -- len Brü -- der sein!
      
      Scha -- la -- la -- la -- la -- la -- la -- la -- la,
      Scha -- la -- la -- la -- la -- la -- la -- la -- la.
    }
    >>
  }
